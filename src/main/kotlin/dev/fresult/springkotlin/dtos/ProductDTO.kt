package dev.fresult.springkotlin.dtos

data class ProductDTO(
  val code: String,
  val name: String
)
