package dev.fresult.springkotlin.converters

import dev.fresult.springkotlin.entities.Product
import jakarta.persistence.AttributeConverter
import jakarta.persistence.Converter
import java.util.*

/* Converter has limitation, that cannot convert...
 * - `id` fields
 * - Relationship fields
 * - @Temporal and @Enumerated fields
 */
@Converter(autoApply = true)
class ProductStatusConverter : AttributeConverter<Product.Status, String> {
  /**
   * @param status the entity attribute value to be converted
   * @return Product's status code
   */
  override fun convertToDatabaseColumn(status: Product.Status): String? {
    return if (Objects.isNull(status)) null else status.getCode()
  }

  /**
   * @param code the data from the database column to be
   * converted
   * @return Product's status
   */
  override fun convertToEntityAttribute(code: String): Product.Status? {
    return if (Objects.isNull(code)) null else Product.Status.codeToStatus(code)
  }
}

