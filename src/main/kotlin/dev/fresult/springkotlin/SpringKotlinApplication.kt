package dev.fresult.springkotlin

import dev.fresult.springkotlin.entities.Order
import dev.fresult.springkotlin.entities.Product
import dev.fresult.springkotlin.entities.embedded.OrderId
import dev.fresult.springkotlin.repositories.OrderRepository
import dev.fresult.springkotlin.repositories.ProductRepository
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import java.util.*

@SpringBootApplication
class SpringKotlinApplication /*: CommandLineRunner*/ {
  @Autowired
  private lateinit var productRepo: ProductRepository

  @Autowired
  private lateinit var orderRepo: OrderRepository

  @Bean
  fun run(vararg args: String?) = CommandLineRunner {
    var product = Product(code = "P001", name = "Coffee", status = Product.Status.APPROVED)

    if (!Objects.isNull(product.aliasNames)) {
      product.aliasNames!!.plusAssign("Java")
      product.aliasNames!!.plusAssign("Cuppa")
      product.aliasNames!!.plusAssign("Caffeine")
    }

    product = productRepo.save(product)

    val order = Order(orderId = OrderId(id = 1L, productId = product.id), quantity = 30)
    orderRepo.save(order)

    log.debug(product)
    log.debug(order)
  }
}

private val log = LogManager.getLogger(SpringKotlinApplication::class.java)
const val INDENT_TAB = "\t\t\t\t\t"

fun main(args: Array<String>) {
  val app = runApplication<SpringKotlinApplication>(*args)
  val env = app.environment

  log.info("SpringKotlinApplication is running on:")
  log.info("${INDENT_TAB}Port: {}", env.getProperty("server.port"))
  log.info("${INDENT_TAB}Context Path: {}", env.getProperty("server.servlet.context-path"))
}
