package dev.fresult.springkotlin.controllers

import dev.fresult.springkotlin.repositories.ProductRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/products")
class ProductController(val productRepository: ProductRepository) {

  @GetMapping
  fun index() = run {
    view(e = productRepository.findAll(), message = "Success")
  }

  @GetMapping("/{id}")
  fun byId(@PathVariable id: Long) = run {
    val product = productRepository.findById(id)

    product.map {
      ResponseEntity.accepted().body(view(message = "Found product (ID: ${it.id})", e = it)).body
    }.orElseGet {
      ResponseEntity.status(HttpStatus.NOT_FOUND).body(view(message = "Not found product", e = null)).body
    }
  }

  fun <T> view(message: String? = "", e: T) = mapOf<String, Any?>("message" to message, "data" to e)
}
