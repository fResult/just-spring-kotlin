package dev.fresult.springkotlin.configs

import io.undertow.UndertowOptions
import org.springframework.boot.web.embedded.undertow.UndertowServletWebServerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class UndertowConfig {
  @Bean
  fun embeddedServletContainerFactory(): UndertowServletWebServerFactory {
    val factory = UndertowServletWebServerFactory()

    factory.addBuilderCustomizers(
        { it.setServerOption(UndertowOptions.ENABLE_HTTP2, true) },
        { it.setIoThreads(Runtime.getRuntime().availableProcessors() * 2) },
        { it.setWorkerThreads(Runtime.getRuntime().availableProcessors() * 10) }
    )

    return factory
  }
}
