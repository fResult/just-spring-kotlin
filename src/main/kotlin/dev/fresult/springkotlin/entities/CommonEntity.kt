package dev.fresult.springkotlin.entities

import jakarta.persistence.EntityListeners
import jakarta.persistence.MappedSuperclass
import jakarta.persistence.Version
import java.time.LocalDateTime

@MappedSuperclass
@EntityListeners(value = [CommonListeners::class])
abstract class CommonEntity {
  var createdDate: LocalDateTime? = null
  var updatedDate: LocalDateTime? = null

  @Version
  private val version = 0
}
