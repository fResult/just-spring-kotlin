package dev.fresult.springkotlin.entities

import jakarta.persistence.PrePersist
import jakarta.persistence.PreUpdate
import java.time.LocalDateTime

class CommonListeners<T : CommonEntity> {
  @PrePersist
  private fun prePersist(e: T) {
    e.createdDate = LocalDateTime.now()
  }

  @PreUpdate
  private fun preUpdate(e: T) {
    e.updatedDate = LocalDateTime.now()
  }
}
