package dev.fresult.springkotlin.entities

import dev.fresult.springkotlin.entities.embedded.OrderId
import jakarta.persistence.*
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.Nationalized

@Entity(name = "orders")
/* @DynamicInsert and @DynamicUpdate are used for reduce SQL INSERT and UPDATE only fields which update
 * - Pro - reduce the SQL code, make query performance better
 * - Con - It will not use SQL in the Hibernate' cached (if not use @DynamicInsert and/or @DynamicUpdate, it will use cached)
 */
@DynamicInsert
@DynamicUpdate
data class Order @JvmOverloads constructor(
  /* idClass
   * Pro: Able to use GeneratedId()
   * Con: Declare many attributes
   */
  // @Id
  // val id: Long = 0,

  // @Id
  // val productId: Long = 0,

  /* EmbeddedId
   * Pro: Declare less attributes
   * Con: Cannot use GeneratedId(). So, we need to manual
   */
  @EmbeddedId
  @AttributeOverrides(
    value = [
      AttributeOverride(name = "id", column = Column(name = "id")),
      AttributeOverride(name = "productId", column = Column(name = "pd_id"))
    ]
  )
  var orderId: OrderId,

  var quantity: Int = 0,

  @Lob
  var details: String? = null,

  @Lob
  @Nationalized
  var nationalDetails: String? = null,

  @Lob
  var images: ByteArray? = null
) : CommonEntity() {
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    val order = other as Order

    if (orderId != order.orderId) return false
    if (quantity != order.quantity) return false
    if (details != order.details) return false
    if (nationalDetails != order.nationalDetails) return false
    if (images != null) {
      if (order.images == null) return false
      if (!images.contentEquals(order.images)) return false
    } else if (order.images != null) return false

    return true
  }

  override fun hashCode(): Int {
    var result = orderId.hashCode()
    result = 31 * result + quantity
    result = 31 * result + (details?.hashCode() ?: 0)
    result = 31 * result + (nationalDetails?.hashCode() ?: 0)
    result = 31 * result + (images?.contentHashCode() ?: 0)
    return result
  }
}
