package dev.fresult.springkotlin.entities.embedded

import jakarta.persistence.Embeddable
import java.io.Serializable

@Embeddable
data class OrderId @JvmOverloads constructor(
  var id: Long? = null,
  var productId: Long? = null,
) : Serializable
