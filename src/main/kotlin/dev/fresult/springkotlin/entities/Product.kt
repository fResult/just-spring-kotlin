package dev.fresult.springkotlin.entities

import jakarta.persistence.*

@Entity(name = "products")
@SequenceGenerator(name = "products_seq")
@Table(
  indexes = [
    Index(name = "products_idx_code_uniq", columnList = "code", unique = true),
    Index(name = "products_idx_status", columnList = "status")
  ]
)
data class Product @JvmOverloads constructor(
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "products_seq")
  var id: Long? = null,
  var code: String? = null,
  var name: String? = null,
  var detail: String? = null,

  @Column(name = "alias_name", length = 20)
  @ElementCollection
  @CollectionTable(name = "products_alias_names", joinColumns = [JoinColumn(name = "product_id")])
  val aliasNames: MutableSet<String>? = mutableSetOf(),

  // @Enumerated(EnumType.STRING)
  // @Convert(converter = ProductStatusConverter::class)
  @Column(length = 1)
  var status: Status? = null
) : CommonEntity() {
  // 2 Tips: field for flagging or calculate something
  // @Transient
  // private var excludedField1: String? = null

  // It's like `private transient String excludedField2;` in Java
  // @kotlin.jvm.Transient
  // private var excludedField2: String? = null

  // 3 Tips: field for flagging or calculate something
  // @Transient private String excludedField1;
  // private transient String excludedField2;
  // private static String excludedField3;

  @PrePersist
  fun prePersist() {
    status = Status.NOT_APPROVED
  }

  enum class Status(
    private val code: String
  ) {
    APPROVED("A"),
    NOT_APPROVED("N"),
    PENDING("P");

    fun getCode(): String {
      return code
    }

    companion object {
      fun codeToStatus(code: String?): Status {
        return entries.find { it.code.equals(code, ignoreCase = true) }
          ?: throw IllegalArgumentException("The code $code is not existed.")
      }
    }
  }
}
