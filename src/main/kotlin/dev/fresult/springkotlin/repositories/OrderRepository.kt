package dev.fresult.springkotlin.repositories

import dev.fresult.springkotlin.entities.Order
import dev.fresult.springkotlin.entities.embedded.OrderId
import jakarta.transaction.Transactional
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
@Transactional
interface OrderRepository : JpaRepository<Order, OrderId> {
}
