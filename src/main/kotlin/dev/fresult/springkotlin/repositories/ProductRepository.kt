package dev.fresult.springkotlin.repositories

import dev.fresult.springkotlin.entities.Product
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional
interface ProductRepository : JpaRepository<Product, Long> {
}
